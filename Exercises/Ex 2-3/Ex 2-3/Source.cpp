#include <iostream>
#include <string>
using namespace std;
int main()
{	
	char walkTo;
	int vertical = 0;
	int horizontal = 0;
	while(true)
	{
		cout << "Current coordinates: (" << vertical << ", " << horizontal <<")" << endl;
		cout << "Which direction do you want to go in? (n,s,e,w)" << endl;
		cin >> walkTo;
		switch (walkTo)
		{
		case 'N':
		case 'n':
			vertical += 1;
			break;
		case 'E':
		case 'e':
			horizontal += 1;
			break;
		case 'S':
		case 's':
			vertical -= 1;
			break;
		case 'W':
		case 'w':
			horizontal -= 1;
			break;
		default:
			cout << "Invalid direction!" << endl;
			break;
		}
	}
	system("pause");
	return 0;
}