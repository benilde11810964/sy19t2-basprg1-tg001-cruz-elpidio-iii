#include <iostream>
#include <string>
using namespace std;
int main()
{
	int firstNumber;
	int secondNumber;
	cout << "Input x:" << endl;
	cin >> firstNumber;
	cout << "Input y:" << endl;
	cin >> secondNumber;

	cout << "Here are some calculations:" << endl;
	cout << firstNumber << " + " << secondNumber << ": ";
	cout << firstNumber + secondNumber << endl;
	cout << firstNumber << " - " << secondNumber << ": ";
	cout << firstNumber - secondNumber << endl;
	cout << firstNumber << " * " << secondNumber << ": ";
	cout << firstNumber * secondNumber << endl;
	cout << firstNumber << " / " << secondNumber << ": ";
	cout << (float)firstNumber / secondNumber << endl;
	cout << firstNumber << " % " << secondNumber << ": ";
	cout << firstNumber % secondNumber << endl;

	system("pause");
	return 0;
}