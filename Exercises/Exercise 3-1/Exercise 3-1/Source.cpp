#include <iostream>
#include <string>
using namespace std;
int main()
{
	int numberOfValues;
	int numberOfValuesCountdown; //we'll subtract this for the loop
	int avgValueCount = 0; // counting down
	int inputValue; // input for the user
	int totalValue=0; // total to be divided by numberOfValues
	float average;
	cout << "Input number of values to be computed:" << endl;
	cin >> numberOfValues;
	numberOfValuesCountdown = numberOfValues;
	while(numberOfValuesCountdown > 0)	
	{
		inputValue = 0;
		avgValueCount += 1;
		numberOfValuesCountdown -= 1;
		cout << "Input value " << avgValueCount << ": ";
		cin >> inputValue;
		totalValue += inputValue;
	} // loop ends here
	cout << "Average: " << totalValue / numberOfValues << endl;
	system("pause");
	return 0;
}