#include <iostream>
#include <string>
using namespace std;
int main()
{
	string items[] = { "RedPotion", "BluePotion", "YggdrasilLeaf", "Elixir", "TeleportScroll", "RedPotion", "RedPotion", "Elixir" };
	int arraySize = 8;
	string search;
	bool searchchecker;
	int itemFoundArray;
	cout << "Search for string:";
	cin >> search;
	for (int i = 0; i < arraySize; i++)
	{
		if (search == items[i])
		{
			searchchecker = 1;
			itemFoundArray = i;
			break;
		}
	}
	if (searchchecker == 1)
	{
		cout << search << " does exist." << endl;
		cout << "It was found at array index " << itemFoundArray << endl;
	}
	else
	{
		cout << search << " does not exist." << endl;
	}
	system("pause");
	return 0;
}