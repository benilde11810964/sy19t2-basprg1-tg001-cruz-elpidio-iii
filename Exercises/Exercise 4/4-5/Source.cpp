#include <iostream>
#include <string>
using namespace std;
int main()
{
	string items[] = { "RedPotion", "BluePotion", "YggdrasilLeaf", "Elixir", "TeleportScroll", "RedPotion", "RedPotion", "Elixir" };
	int arraySize = 8;
	string search;
	bool searchchecker;
	int itemFoundArray = 0;
	cout << "Search for string:";
	cin >> search;
	for (int i = 0; i < arraySize; i++)
	{
		if (search == items[i])
		{
			searchchecker = 1;
			itemFoundArray += 1;
		}
	}
	if (searchchecker == 1)
	{
		cout << search << " does exist." << endl;
		cout << "You have " << itemFoundArray << " " << search << endl;
	}
	else
	{
		cout << search << " does not exist." << endl;
	}
	system("pause");
	return 0;
}