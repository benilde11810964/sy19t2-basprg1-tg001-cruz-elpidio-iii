#include <iostream>
#include <string>
using namespace std;
int main()
{
	int numbers[10] = { -20, 0, 7, 3, 2, 1, 8, -3, 6, 3 };
	int arraySize = 10;
	int largestNumber = numbers[0];
	for (int i = 0; i < arraySize; i++) 
	{
		if (largestNumber < numbers[i]) 
		{
			largestNumber = numbers[i];
		}
	}
	cout << largestNumber << endl;
	system("pause");
	return 0;
}