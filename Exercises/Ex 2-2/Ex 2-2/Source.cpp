#include <iostream>
#include <string>

using namespace std;

int main()
{
	int currentHP;
	int maxHP;
	

	cout << "How much MAXIMUM HP do you have?" << endl;
	cin >> maxHP;
	cout << "How much CURRENT HP do you have?" << endl;
	cin >> currentHP;

	float hpPercentage = ((float)currentHP / (float)maxHP) * 100;

	cout <<"This is your current HP percent: "<< hpPercentage << "%"<< endl;
	if (hpPercentage==100)
	{
		cout << "You're at full health!" << endl;
	}
	else if (hpPercentage >= 50 && hpPercentage <= 99)
	{
		cout << "You're at green health!" << endl;
	}
	else if (hpPercentage >=20 && hpPercentage <=49)
	{
		cout << "You're at yellow health!" << endl;
	}
	else if (hpPercentage >=1 && hpPercentage <= 19)
	{
		cout << "You're at red health; get a medic, man!" << endl;
	}
	else if (hpPercentage==0)
	{
		cout << "You're actually dead, dude." << endl;
	}
	else if (hpPercentage >= 101)
	{
		cout << "Holy kahoonas, you're overhealed or something!" << endl;
	}
	else if (hpPercentage < 0)
	{
		cout << "Dude! You're deader than dead!" << endl;
	}
	system("pause");
	return 0;
}