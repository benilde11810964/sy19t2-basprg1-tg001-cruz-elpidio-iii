#include <iostream>
#include <string>
using namespace std;
int main()
{
	int factorialInput;
	int factorial = 1;

	cout << "Input your number you wish to be factorial'd: ";
	cin >> factorialInput;
	if (factorialInput != 0)
	{
		for (int factorialCount = 1; factorialCount <= factorialInput; factorialCount++)
		{
			factorial *= factorialCount;
		}
	}
	else //this is what happens if your input is 0
	{
		cout << "The result is: 1" << endl;
		system("pause");
		return 0;
	}
	cout << "The result is: " << factorial << endl;
	system("pause");
	return 0;
}