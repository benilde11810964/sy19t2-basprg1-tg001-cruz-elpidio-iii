#include <iostream>
#include <string>

using namespace std;

int main()
{
	string name;
	string lastName;
	int age;
	int moneyCount;
	string breakfast;
	bool breakfastChecker = 0;

	cout << "What is your first name?" << endl;
	cin >> name;
	cout << "What is your last name?" << endl;
	cin >> lastName;
	cout << "How old are you?" << endl;
	cin >> age;
	cout << "How much money do you have?" << endl;
	cin >> moneyCount;
	cout << "Did you eat breakfast yet? Answer in Yes or No (case-sensitive)." << endl;
	while (breakfastChecker == 0)
	{
		cin >> breakfast;
		if (breakfast == "Yes")
		{
			breakfastChecker += 1;
		}
		else if (breakfast == "No")
		{
			breakfastChecker += 1;
		}
		else
		{
			cout << "Answer in Yes or No (case-sensitive)." << endl;
		}
	}
	cout << "Here are your results:" << endl;
	cout << "Full name: " << name << " " << lastName << endl;
	cout << "Age: " << age << endl;
	cout << "Money: " << moneyCount << endl;
	cout << "Has eaten breakfast: " << breakfast << endl;

	system("pause");
	return 0;
}