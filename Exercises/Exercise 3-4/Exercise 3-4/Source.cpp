#include <iostream>
#include <string>
using namespace std;
int main()
{
	int numberOfTerms;
	int oddNumberOfTerms = 0;
	cout << "Input number of terms: ";
	cin >> numberOfTerms;
	for (int numberSequence = 1; oddNumberOfTerms < numberOfTerms; numberSequence++)
	{
		int startingNum = 0;
		startingNum += numberSequence;
		if (startingNum % 2 == 0)
		{
			cout << " ";
		}
		else
		{
			cout << startingNum;
			oddNumberOfTerms += 1;
		}
	}
	system("pause");
	return 0;
}