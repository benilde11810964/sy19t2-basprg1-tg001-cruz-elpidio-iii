#include <iostream>
#include <string>
#include <time.h>
using namespace std;
int main()
{
	int playerHP;
	int playerMinimumDamage;
	int playerMaximumDamage;
	int playerAction;
	int playerRandomDamage;
	int enemyHP;
	int enemyMinimumDamage;
	int enemyMaximumDamage;
	int enemyRandomDamage;
	srand(time(0));
	//declare variables

	cout << "What's the Player's HP?" << endl;
	cin >> playerHP;
	cout << "What's the Player's Minimum Damage?" << endl;
	cin >> playerMinimumDamage;
	cout << "What's the Player's Maximum Damage?" << endl;
	cin >> playerMaximumDamage;
	cout << "What's the Enemy's HP?" << endl;
	cin >> enemyHP;
	cout << "What's the Enemey's Minimum Damage?" << endl;
	cin >> enemyMinimumDamage;
	cout << "What's the Enemy's Maximum Damage?" << endl;
	cin >> enemyMaximumDamage;

	cout << "Oh s**t son! A nobleman wants you dead! Defend yourself!" << endl;

	while (playerHP > 0 && enemyHP > 0)
	{
		playerAction = 0;
		int enemyAction = rand() % 3 + 1;
		// for reference, 1 - Attack 2 - Wild Attack 3 - Defend
		playerRandomDamage = rand() % (playerMaximumDamage - playerMinimumDamage + 1) + playerMinimumDamage;
		enemyRandomDamage = rand() % (enemyMaximumDamage - enemyMinimumDamage + 1) + enemyMinimumDamage;
		cout << "============================" << endl;
		cout << "Your health: " << playerHP << endl;
		cout << "Enemy's health: " << enemyHP << endl;
		cout << "What's your move?" << endl;
		cout << "1 - Attack" << endl;
		cout << "2 - Wild Attack" << endl;
		cout << "3 - Defend" << endl;
		cin >> playerAction;
		switch (playerAction)
		{
		case 1:
			cout << "You're Attacking!" << endl;
			if (enemyAction == 1)
			{
				cout << "The enemy Attacks!" << endl;
				enemyHP -= playerRandomDamage;
				cout << "You deal " << playerRandomDamage << " to the enemy! Enemy's health is now: " << enemyHP << endl;
				playerHP -= enemyRandomDamage;
				cout << "The enemy deals " << enemyRandomDamage << " to you! Your health is now: " << playerHP << endl;
			}
			else if (enemyAction == 2)
			{
				cout << "The enemy Wild Attacks!" << endl;
				enemyHP -= playerRandomDamage;
				cout << "You deal " << playerRandomDamage << " to the enemy! Enemy's health is now: " << enemyHP << endl;
				playerHP -= (enemyRandomDamage * 2);
				cout << "The enemy deals a WILD " << enemyRandomDamage * 2 << " to you! Your health is now: " << playerHP << endl;
			}
			else if (enemyAction == 3)
			{
				cout << "The enemy Defends!" << endl;
				enemyHP -= (playerRandomDamage * .5);
				cout << "You deal a defended " << playerRandomDamage * .5 << " to the enemy! Enemy's health is now: " << enemyHP << endl;
			}
			break;
		case 2:
			cout << "You're Wild Attacking!" << endl;
			if (enemyAction == 1)
			{
				cout << "The enemy Attacks!" << endl;
				enemyHP -= playerRandomDamage * 2;
				cout << "You deal a WILD " << playerRandomDamage * 2 << " to the enemy! Enemy's health is now: " << enemyHP << endl;
				playerHP -= enemyRandomDamage;
				cout << "The enemy deals " << enemyRandomDamage << " to you! Your health is now: " << playerHP << endl;
			}
			else if (enemyAction == 2)
			{
				cout << "The enemy Wild Attacks!" << endl;
				enemyHP -= playerRandomDamage * 2;
				cout << "You deal a WILD " << playerRandomDamage * 2 << " to the enemy! Enemy's health is now: " << enemyHP << endl;
				playerHP -= (enemyRandomDamage * 2);
				cout << "The enemy deals a WILD " << enemyRandomDamage * 2 << " to you! Your health is now: " << playerHP << endl;
				cout << "It's a SLUG-FEST in here!" << endl;
			}
			else if (enemyAction == 3)
			{
				cout << "The enemy Defends!" << endl;
				playerHP -= (enemyRandomDamage * 2);
				cout << "Oh f**k! You're parried and the enemy ripostes for: " << enemyRandomDamage * 2 << "! Your health is now: " << playerHP << endl;
			}
			break;
		case 3:
			cout << "You're Defending!" << endl;
			if (enemyAction == 1)
			{
				cout << "The enemy Attacks!" << endl;
				playerHP -= (enemyRandomDamage * .5);
				cout << "The enemy deals a defended " << enemyRandomDamage * .5 << " to you! Your health is now: " << playerHP << endl;
			}
			else if (enemyAction == 2)
			{
				cout << "The enemy Wild Attacks!" << endl;
				enemyHP -= (playerRandomDamage * 2);
				cout << "You defend against the enemy's Wild Attack and riposte for: " << (playerRandomDamage * 2) << "! The enemy's current health is: " << enemyHP << endl;
			}
			else if (enemyAction == 3)
			{
				cout << "The enemy Defends!" << endl;
				cout << "...Nothing quite happens. Both of you stare each other down. This got awkward real quick." << endl;
			}

			break;
		default:
			cout << "Wrong input!" << endl;
			break;
		}
	}

	if (playerHP <= 0 && enemyHP <= 0) //tiebreaker first
	{
		cout << "It seems to be a tie! It's a draw!" << endl;
	}
	else if (enemyHP <= 0)
	{
		cout << "You are victorious!" << endl;
	}
	else if (playerHP <= 0)
	{
		cout << "You lost!" << endl;
	}
	system("pause");
	return 0;
}