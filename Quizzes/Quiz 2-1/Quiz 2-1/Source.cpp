#include <iostream>

using namespace std;

int main()
{
	int radius;
	float pi = 3.14;
	//pi represented by 3.14
	cout << "Input radius." << endl;
	cin >> radius;
	cout << "Area: " << pi * (radius * radius) << endl;
	cout << "Circumference: " << 2 * pi * radius << endl;
	system("pause");
	return 0;
}