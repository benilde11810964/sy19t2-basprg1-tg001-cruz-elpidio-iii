#include <iostream>
#include <string>
#include <ctime>
using namespace std;
int main()
{
	int attempts = 1;

	while (true)
	{
		srand(time(0));
		int dice1 = rand() % 6 + 1;
		cout << dice1 << ",";
		int dice2 = rand() % 6 + 1;
		cout << dice2 << " = "<<dice1+dice2<< endl;
		if ((dice1 + dice2) % 4 == 0)
		{
			cout << "You rolled " << attempts << " times." << endl;
			break;
		}
		else 
		{
			attempts++;
			system("pause");
		}
	}
	system("pause");
	return 0;
}