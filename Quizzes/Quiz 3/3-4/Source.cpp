#include <iostream>
#include <string>
using namespace std;
int main()
{
	int inputNumber;
	int firstFibonacciNumber = 1;
	int secondFibonacciNumber = 1;
	cout << "Enter fibonacci number sequence: " << endl;
	cin >> inputNumber;
	for (int startingNumber = 0; startingNumber < inputNumber; startingNumber++)
	{
		cout << firstFibonacciNumber << " ";
		cout << secondFibonacciNumber << " ";
		startingNumber++;
		firstFibonacciNumber += secondFibonacciNumber;
		secondFibonacciNumber += firstFibonacciNumber;
	}
	system("pause");
	return 0;
}