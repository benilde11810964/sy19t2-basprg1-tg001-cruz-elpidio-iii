#include <iostream>
#include <string>
using namespace std;
int main()
{
	string input;
	cout << "Input text to reverse:";
	cin >> input;
	int inputLength = input.length();
	for (int i = inputLength; i > 0; i--)
	{
		cout << input[i-1];
	}
	cout << endl;
	system("pause");
	return 0;
}