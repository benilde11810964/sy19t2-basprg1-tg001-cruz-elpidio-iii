#include <iostream>
#include <string>
#include <time.h>
using namespace std;
int main()
{
	
	int minimumDamage;
	int maximumDamage;
	cout << "What's your minimum damage?" << endl;
	cin >> minimumDamage;
	cout << "What's your maximum damage?" << endl;
	cin >> maximumDamage;
	int range = maximumDamage - minimumDamage;
	srand(time(0));
	int randomDamage = rand() % range + minimumDamage;
	cout << randomDamage << endl;
	system("pause");
	return 0;
}