#include <iostream>
#include <string>
#include <time.h>
using namespace std;
int main()
{
	srand(time(0));
	bool duplicateChecker = 0;
	int numbers[20];
	for (int i = 0; i < 20; i++) 
	{
		numbers[i] = rand() % 100 + 1;
	}
	for (int i = 0; i < 20; i++)
	{
		cout << numbers[i] << endl;

	}
	for (int i = 0; i < 20; ++i)
	{
		if (numbers[0] < numbers[i])
		{
			int storedvalue = numbers[0];
			numbers[0] = numbers[i];
			numbers[i] = storedvalue;
		}
	}
	for (int i = 1; i < 20; ++i)
	{
		if (numbers[1] < numbers[i])
		{
			int storedvalue = numbers[1];
			numbers[1] = numbers[i];
			numbers[i] = storedvalue;
		}
	}
	cout << "Largest element = " << numbers[0] << endl;
	cout << "Second largest element = " << numbers[1] << endl;
	for (int i = 0; i < 20; i++) 
	{
		for (int j = i + 1; j < 20; j++) 
		{
			if (numbers[i] == numbers[j])
			{
				cout << "Duplicate spotted: " << numbers[i] << endl;
				break;
			}
		}
	}
	system("pause");
	return 0;
}